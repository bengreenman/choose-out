#lang scribble/manual

@require[
  scribble/example
  scribble-abbrevs/manual
  (for-label racket/base
             (only-in racket/contract/base -> any/c)
             choose-out choose-out/renaming)]

@title{Choose-out: Conditional Provide}

@defmodule[choose-out]{
}

@defform[#:kind "provide-spec" (choose-out proc-expr [true-id false-id] ...)]{
  Exports the names @racket[true-id].
  The expression @racket[proc-expr] must evaluate at expansion-time to a zero-argument
   procedure.
  Each name is bound to a @tech[#:key "choose-out renaming"]{transformer}
   that conditionally expands to @racket[true-id] or @racket[false-id]
   depending on the result of the given procedure in the importing module.

  @examples[#:eval (make-base-eval)
    (module struct-math racket
      (code:comment "`num` is an internal representation")
      (struct num [value])

      (code:comment "set this to `#t` in modules that use `num` values")
      (define-for-syntax num-client? (box #false))

      (code:comment "addition for `num` values")
      (define (num-+ n0 n1)
        (num (+ (num-value n0) (num-value n1))))

      (code:comment "wrapper: convert `num` addition to Racket addition")
      (define (racket-+ v0 v1)
        (num-value (num-+ (num v0) (num v1))))

      (code:comment "export `num-+` or `racket-+` depending on the importing module")
      (require choose-out)
      (provide
        num
        (for-syntax num-client?)
        (choose-out (λ () (unbox num-client?))
          [num-+ racket-+]))
    )

    (module struct-client racket
      (code:comment "familiar client, uses the `num` representation")
      (require 'struct-math)
      (begin-for-syntax (set-box! num-client? #true))
      (displayln (num-+ (num 2) (num 2))))
    (require 'struct-client)

    (module racket-client racket
      (code:comment "foreign client: uses Racket numbers")
      (require 'struct-math)
      (displayln (num-+ 2 2)))
    (require 'racket-client)
  ]
}

Typed Racket uses a similar protocol to decide whether an identifier exported
 from a typed module should be protected with a contract.
A @hash-lang[] @racketmodname[typed/racket] module expands (via @racket[#%module-begin])
 to code that sets a flag similar to the @racket[num-client?] flag in the example
 above.


@; -----------------------------------------------------------------------------
@section{Choose-Out Renamings}
@defmodule[choose-out/renaming]{
  A @deftech{choose-out renaming} is a kind of @tech/reference{rename transformer}.
}

@defproc[(make-choose-out-renaming [rename? (-> any/c)] [true-id identifier?] [false-id identifier?]) choose-out-renaming?]{
  Construct a @tech{choose-out renaming} from a predicate and two identifiers.
  Adds the @racket['not-free-identifier=?] syntax property to both identifiers.
}

@defproc[(choose-out-renaming? [x any/c]) boolean?]{
  Predicate for a @tech{choose-out renaming}.
}

@defproc[(undo-choose-out-renaming [id identifier?]) identifier?]{
  If @racket[id] is bound to a @tech{choose-out renaming},
   returns the source of the renaming.
  Otherwise, returns @racket[id].
}
