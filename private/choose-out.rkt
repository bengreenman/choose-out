#lang racket/base

(provide
  choose-out)

(require
  (for-syntax
    racket/base
    syntax/parse
    racket/provide-transform
    choose-out/private/renaming))

;; =============================================================================

(define-syntax choose-out
  (make-provide-pre-transformer
    (λ (stx modes)
      (syntax-parse stx
        [(_ ?pre-f:expr [?true-id*:id ?false-id*:id] ...)
         #:with (?wrap-id* ...) (generate-temporaries #'(?true-id* ...))
         #:do
           [(syntax-local-lift-module-end-declaration
              #'(define-syntaxes (?wrap-id* ...)
                  (let ([?f ?pre-f])
                    (values (make-choose-out-renaming ?f #'?true-id* #'?false-id*) ...))))]
         (pre-expand-export
           #'(rename-out [?wrap-id* ?true-id*] ...)
           modes)]))))
