choose-out
===
[![Scribble](https://img.shields.io/badge/Docs-Scribble-blue.svg)](http://docs.racket-lang.org/choose-out/index.html)

Conditional `provide` form for Racket.

Example:

```
(provide (choose-out f [a b]))
;; Exports the name `a`.
;; In an importing module, the name expands to:
;;  `#`(if #,(f) #'a #'b)`
;; Note that `f` must evaluate to a procedure at expansion-time.
```


History
---

Inspired by Typed Racket's protocol for protecting typed identifiers exported to untyped modules.
